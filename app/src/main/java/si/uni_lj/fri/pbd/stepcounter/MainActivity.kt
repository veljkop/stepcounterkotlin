package si.uni_lj.fri.pbd.stepcounter

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import si.uni_lj.fri.pbd.stepcounter.databinding.ActivityMainBinding

// TODO: declare sensorManager and sensor variables
class MainActivity : AppCompatActivity(), SensorEventListener {

    // TODO: declare sensorManager and sensor variables


    // TODO: declare step count variables


    companion object {
        const val TAG = "MainActivity"
        const val ACTIVITY_REQUEST_CODE = 42
    }

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        // TODO: instantiate sensorManager


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            setupPermissions()
        } else {
            setSensor()
        }

        // TODO: Set onClickListener for the reset button

    }

    fun setSensor() {
        // TODO: check whether TYPE_STEP_COUNTER sensor is there

    }


    override fun onResume() {
        super.onResume()
        // TODO: register listener
    }

    override fun onPause() {
        super.onPause()
        // TODO: unregister listener

    }

    // TODO: Override functions



    @RequiresApi(Build.VERSION_CODES.Q)
    private fun setupPermissions() {

        val permission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACTIVITY_RECOGNITION)
        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Permission to track activity denied")

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACTIVITY_RECOGNITION)){

                val builder = AlertDialog.Builder(this)
                with(builder){
                    setMessage("I need this permission to recognise your steps!")
                    setTitle("Permission I really need")
                    setPositiveButton("OK"){p0, p1->
                        makeRequest()
                    }
                }

                val dialog = builder.create()
                dialog.show()

            } else {
                makeRequest()
            }
        } else {
            setSensor()
        }
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun makeRequest() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACTIVITY_RECOGNITION), ACTIVITY_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Permission was denied")
        } else {
            Log.d(TAG, "Permission was granted")
            setSensor()
        }
    }



}